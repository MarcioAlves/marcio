import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, HostListener, ViewChild } from '@angular/core';
import { fadeAnimation } from './animation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    fadeAnimation,
    trigger('toggleMenuButton', [
      transition(':enter', [
        style({
          opacity: 0,
          width: '0px',
          position: 'relative' 
        }),
        animate('.3s', style({
          opacity: 1, 
          width: '64px',
          position: 'relative'
        })),
      ]),
      transition(':leave', [
        style({
          opacity: 1,
          width: '64px',
          position: 'relative' 
        }),
        animate('.2s', style({
          opacity: 0,
          width: '0px',
          position: 'relative'
        })),
      ])
    ]),
    
    trigger('toggleMainMenu', [
      transition(':enter', [
        style({
          top: '-100px',
          position: 'relative' 
        }),
        animate('.3s', 
          style({
            top: '0px',
            position: 'relative'
        })),
      ]),
      transition(':leave', [
        style({ 
          top: '0px',
          position: 'relative' 
        }),
        animate('.2s', style({
          top: '-100px',
          position: 'relative'
        })),
      ])
    ])
  ]
})
export class AppComponent {
  title = 'Marcio Alves';
  smallScreen: boolean = window.innerWidth <= 630;
  @ViewChild('sideNav') sidenav: any;

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.smallScreen = window.innerWidth <= 630;
    this.checkSideNav();
  }

  checkSideNav() {
    if(!this.smallScreen) {
      this.sidenav.close();
    }
  }
}
